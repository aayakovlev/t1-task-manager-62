package ru.t1.aayakovlev.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.dto.model.TaskDTO;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class TaskShowAllResponse extends AbstractTaskResponse {

    public TaskShowAllResponse(@Nullable final List<TaskDTO> tasks) {
        super(tasks);
    }

}
