package ru.t1.aayakovlev.tm.service.model.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AuthenticationException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.model.AbstractUserOwnedModel;
import ru.t1.aayakovlev.tm.repository.model.ExtendedRepository;
import ru.t1.aayakovlev.tm.service.model.ExtendedService;
import ru.t1.aayakovlev.tm.service.model.UserService;

import java.util.*;

@Service
public abstract class AbstractExtendedService<E extends AbstractUserOwnedModel, R extends ExtendedRepository<E>>
        extends AbstractBaseService<E, R> implements ExtendedService<E> {

    protected abstract @NotNull ExtendedRepository<E> getRepository();

    @NotNull
    @Autowired
    private UserService userService;

    @NotNull
    @Override
    @Transactional
    public E save(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        model.setUser(userService.findById(userId));
        return getRepository().save(model);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<E> add(@Nullable final String userId, @Nullable final Collection<E> models)
            throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<E> resultEntities = new ArrayList<>();
        for (@NotNull final E entity : models) {
            entity.setUser(userService.findById(userId));
            @NotNull final E resultEntity = getRepository().save(entity);
            resultEntities.add(resultEntity);
        }
        return resultEntities;
    }

    @NotNull
    @Override
    @Transactional
    public E update(@Nullable final String userId, @Nullable final E model) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthenticationException();
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(userId, model.getId())) throw new EntityNotFoundException();
        return update(model);
    }

}
