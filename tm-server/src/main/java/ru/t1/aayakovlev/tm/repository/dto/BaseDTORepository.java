package ru.t1.aayakovlev.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1.aayakovlev.tm.dto.model.AbstractModelDTO;

@Repository
@Scope("prototype")
public interface BaseDTORepository<E extends AbstractModelDTO> extends JpaRepository<E, String> {

}
