package ru.t1.aayakovlev.tm.service.dto.impl;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.aayakovlev.tm.dto.model.UserDTO;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.entity.UserEmailExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserLoginExistsException;
import ru.t1.aayakovlev.tm.exception.entity.UserNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.EmailEmptyException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.exception.field.RoleEmptyException;
import ru.t1.aayakovlev.tm.repository.dto.ProjectDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.SessionDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.TaskDTORepository;
import ru.t1.aayakovlev.tm.repository.dto.UserDTORepository;
import ru.t1.aayakovlev.tm.service.PropertyService;
import ru.t1.aayakovlev.tm.service.dto.UserDTOService;
import ru.t1.aayakovlev.tm.util.HashUtil;

@Service
public class UserDTOServiceImpl extends AbstractBaseDTOService<UserDTO, UserDTORepository> implements UserDTOService {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private UserDTORepository repository;

    @Getter
    @NotNull
    @Autowired
    private ProjectDTORepository projectRepository;

    @Getter
    @NotNull
    @Autowired
    private TaskDTORepository taskRepository;

    @Getter
    @NotNull
    @Autowired
    private SessionDTORepository sessionRepository;

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        if (isEmailExists(login)) throw new UserEmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setEmail(email);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws Exception {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new UserLoginExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setRole(role);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return getRepository().save(user);
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return getRepository().findByLogin(login);
    }

    @Nullable
    @Override
    public UserDTO findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return getRepository().findByEmail(email);
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return getRepository().existsByLogin(login);
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return getRepository().existsByEmail(email);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        resultUser.setLocked(true);
        return update(resultUser);
    }

    @Override
    @Transactional
    public void remove(@Nullable final UserDTO model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        if (!existsById(model.getId())) throw new EntityNotFoundException();
        getTaskRepository().deleteAllByUserId(model.getId());
        getProjectRepository().deleteAllByUserId(model.getId());
        getSessionRepository().deleteAllByUserId(model.getId());
        getRepository().deleteById(model.getId());
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultModel = findByLogin(login);
        remove(resultModel);
    }

    @Override
    @Transactional
    public void removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO resultModel = findByEmail(email);
        remove(resultModel);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO resultUser = findById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        return update(resultUser);
    }

    @NotNull
    @Override
    @Transactional
    public UserDTO unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO resultUser = findByLogin(login);
        if (resultUser == null) throw new UserNotFoundException();
        resultUser.setLocked(false);
        return update(resultUser);
    }

    @Override
    @Transactional
    public @NotNull UserDTO update(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        @NotNull final UserDTO user = findById(id);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return update(user);
    }

}
