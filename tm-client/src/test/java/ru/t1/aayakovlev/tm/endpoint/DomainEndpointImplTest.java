//package ru.t1.aayakovlev.tm.endpoint;
//
//import org.jetbrains.annotations.NotNull;
//import org.jetbrains.annotations.Nullable;
//import org.junit.After;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.experimental.categories.Category;
//import ru.t1.aayakovlev.tm.dto.request.*;
//import ru.t1.aayakovlev.tm.exception.AbstractException;
//import ru.t1.aayakovlev.tm.marker.IntegrationCategory;
//import ru.t1.aayakovlev.tm.service.PropertyService;
//import ru.t1.aayakovlev.tm.service.impl.PropertyServiceImpl;
//
//import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.ADMIN_LOGIN;
//import static ru.t1.aayakovlev.tm.constant.UserEndpointTestConstant.ADMIN_PASSWORD;
//
//@Category(IntegrationCategory.class)
//public final class DomainEndpointImplTest {
//
//
//    @NotNull
//    private final PropertyService propertyService = new PropertyServiceImpl();
//
//    @NotNull
//    private final AuthEndpoint authEndpoint = AuthEndpoint.newInstance(propertyService);
//
//    @NotNull
//    private final DomainEndpoint domainEndpoint = DomainEndpoint.newInstance(propertyService);
//
//    @Nullable
//    private String adminToken;
//
//    @Before
//    public void init() throws AbstractException {
//        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
//        loginRequest.setLogin(ADMIN_LOGIN);
//        loginRequest.setPassword(ADMIN_PASSWORD);
//        adminToken = authEndpoint.login(loginRequest).getToken();
//    }
//
//    @After
//    public void after() {
//    }
//
//    @Test
//    public void When_BackupLoad_Expect_BackupLoadLoadResponse() throws AbstractException {
//        domainEndpoint.backupSave(new DataBackupSaveRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.backupLoad(new DataBackupLoadRequest(adminToken)));
//    }
//
//    @Test
//    public void When_BackupSave_Expect_DataBackupSaveResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.backupSave(new DataBackupSaveRequest(adminToken)));
//    }
//
//    @Test
//    public void When_Base64Load_Expect_DataBase64LoadResponse() throws AbstractException {
//        domainEndpoint.base64Save(new DataBase64SaveRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.base64Load(new DataBase64LoadRequest(adminToken)));
//    }
//
//    @Test
//    public void When_Base64Save_Expect_DataBase64SaveResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.base64Save(new DataBase64SaveRequest(adminToken)));
//    }
//
//    @Test
//    public void When_BinaryLoad_Expect_DataBinaryLoadResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.binarySave(new DataBinarySaveRequest(adminToken)));
//    }
//
//    @Test
//    public void When_BinarySave_Expect_DataBinarySaveResponse() throws AbstractException {
//        domainEndpoint.binarySave(new DataBinarySaveRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.binaryLoad(new DataBinaryLoadRequest(adminToken)));
//    }
//
//    @Test
//    public void When_JsonLoadFXml_Expect_DataJsonLoadFXmlResponse() throws AbstractException {
//        domainEndpoint.jsonSaveFXml(new DataJsonSaveFasterXmlRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.jsonLoadFXml(new DataJsonLoadFasterXmlRequest(adminToken)));
//    }
//
//    @Test
//    public void When_JsonLoadJaxB_Expect_DataJsonLoadJaxBResponse() throws AbstractException {
//        domainEndpoint.jsonSaveJaxB(new DataJsonSaveJaxBRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.jsonLoadJaxB(new DataJsonLoadJaxBRequest(adminToken)));
//    }
//
//    @Test
//    public void When_JsonSaveFXml_Expect_DataJsonSaveFXmlResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.jsonSaveFXml(new DataJsonSaveFasterXmlRequest(adminToken)));
//    }
//
//    @Test
//    public void When_JsonSaveJaxB_Expect_DataJsonSaveJaxBResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.jsonSaveJaxB(new DataJsonSaveJaxBRequest(adminToken)));
//    }
//
//    @Test
//    public void When_XmlLoadFXml_Expect_DataXmlLoadFXmlResponse() throws AbstractException {
//        domainEndpoint.xmlSaveFXml(new DataXmlSaveFasterXmlRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.xmlLoadFXml(new DataXmlLoadFasterXmlRequest(adminToken)));
//    }
//
//    @Test
//    public void When_XmlLoadJaxB_Expect_DataXmlLoadJaxBResponse() throws AbstractException {
//        domainEndpoint.xmlSaveJaxB(new DataXmlSaveJaxBRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.xmlLoadJaxB(new DataXmlLoadJaxBRequest(adminToken)));
//    }
//
//    @Test
//    public void When_XmlSaveFXml_Expect_DataXmlSaveFXmlResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.xmlSaveFXml(new DataXmlSaveFasterXmlRequest(adminToken)));
//    }
//
//    @Test
//    public void When_XmlSaveJaxB_Expect_DataXmlSaveJaxBResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.xmlSaveJaxB(new DataXmlSaveJaxBRequest(adminToken)));
//    }
//
//    @Test
//    public void When_YamlLoad_Expect_DataYamlLoadResponse() throws AbstractException {
//        domainEndpoint.yamlSave(new DataYamlSaveFasterXmlRequest(adminToken));
//        Assert.assertNotNull(domainEndpoint.yamlLoad(new DataYamlLoadFasterXmlRequest(adminToken)));
//    }
//
//    @Test
//    public void When_YamlSave_Expect_DataYamlSaveResponse() throws AbstractException {
//        Assert.assertNotNull(domainEndpoint.yamlSave(new DataYamlSaveFasterXmlRequest(adminToken)));
//    }
//
//}
