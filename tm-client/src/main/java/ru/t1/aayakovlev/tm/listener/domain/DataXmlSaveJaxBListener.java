package ru.t1.aayakovlev.tm.listener.domain;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataXmlSaveJaxBRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataXmlSaveJaxBListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-xml-jaxb-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to xml file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveJaxBListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE XML]");
        domainEndpoint.xmlSaveJaxB(new DataXmlSaveJaxBRequest(getToken()));
    }

}
