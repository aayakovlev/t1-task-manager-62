package ru.t1.aayakovlev.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.UserLockRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

@Component
public final class UserLockListener extends AbstractUserListener {

    @NotNull
    public static final String DESCRIPTION = "Lock user by login.";

    @NotNull
    public static final String NAME = "user-lock";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userLockListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[USER LOCK]");
        System.out.print("Enter login: ");
        @NotNull final String login = nextLine();

        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);

        userEndpoint.lock(request);
    }

}
