package ru.t1.aayakovlev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.ServerHostnameRequest;
import ru.t1.aayakovlev.tm.dto.response.ServerHostnameResponse;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class SystemShowHostnameListener extends AbstractSystemListener {

    @NotNull
    public static final String DESCRIPTION = "Show server's response hostname.";

    @NotNull
    public static final String NAME = "hostname";

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@systemShowHostnameListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[HOSTNAME]");

        @NotNull final ServerHostnameRequest request = new ServerHostnameRequest();
        @Nullable final ServerHostnameResponse response = systemEndpoint.getHostname(request);

        System.out.println(response.getHostname());
    }

}
