package ru.t1.aayakovlev.tm.listener.domain;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aayakovlev.tm.dto.request.DataBinarySaveRequest;
import ru.t1.aayakovlev.tm.event.ConsoleEvent;
import ru.t1.aayakovlev.tm.exception.AbstractException;

@Component
public final class DataBinarySaveListener extends AbstractDataListener {

    @NotNull
    public static final String NAME = "data-bin-save";

    @NotNull
    private static final String DESCRIPTION = "Save data to binary file.";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@dataBinarySaveListener.name() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE BINARY]");
        domainEndpoint.binarySave(new DataBinarySaveRequest(getToken()));
    }

}
